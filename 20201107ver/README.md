# IKBAEB-th - Thai keyboard layout 20201107

`IKBAEB-th` (Thai: อี๊กแบ็บ, <small>pronounced </small>[\[íːkbɛ̀p\]](https://en.wikipedia.org/wiki/Help:IPA/Thai), [iikᴴ-baepᴸ](http://thai-language.com/ref)) is an alternative Thai keyboard layout unlike [Kedmanee (เกษมณี)](https://en.wikipedia.org/wiki/Thai_Kedmanee_keyboard_layout) or [Pattachote (ปัตตะโชติ)](https://en.wikipedia.org/wiki/Thai_Pattachote_keyboard_layout).

![IKBAEB-th layout 00](ikbatha0.png)
*ikbatha0 : Digits row look like [Bitpaired](https://en.wikipedia.org/wiki/Bit-paired_keyboard)*
![IKBAEB-th layout 01](ikbatha1.png)
*ikbatha1 : Digits row look like US-Qwerty*

This layout optimized for:

- Less stress on the right pinky reducing pain
- Finger-rolling combos inspired by [Colemak](https://colemak.com/)
- Maintaining Arabic numeral row for everyday typing, with the lesser-used Thai numerals under [AltGr](https://en.wikipedia.org/wiki/AltGr_key) in the same order
- etc.

The name IKBAEB originates from the Thai word, อีกแบบ, meaning _alternative_. However "อีกแบบ" is too ordinary, so it will be be called "อี๊กแบ็บ".

# SUGGEST

I prefer shift-key on thumb location. 

Use something like [xkeysnail](https://github.com/mooz/xkeysnail) to make [Dual-role keys](https://en.wikipedia.org/wiki/Modifier_key#Dual-role_keys) ([`space-bar`and`shift`](https://poyo.hatenablog.jp/entry/2018/06/23/020147#SandS)) if you don't have `extra shift` key.  
